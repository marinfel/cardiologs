import pandas as pd
from dateutil import relativedelta, parser
import datetime

# The header allow Pandas to parse nicely the file and get a dataframe from it.
# I did this since I didn't know the maximum number of tags (but 3 is the max in the supplied file)
HEADER = ['type', 'start', 'stop', 'tag1', 'tag2', 'tag3']
# Time range that will be used to compute "instant frequencies" (minutes)
INSTANT_FREQ_RANGE = 10


def print_report(results, indent=1):
    """
    Prints nicely the dictionary storing the final results
    """
    if indent == 1:
        print('\n\n')
    for key, value in results.items():
        if isinstance(value, dict):
            print('\t'*indent + key)
            print_report(value, indent+1)
        else:
            print(''.join(['\t'*(indent), key, ' : ', str(value)]))

def get_premature_data(df):
    """
    Returns a dictionary with the number of premature P and QRS waves.
    Gets all rows tagged premature, groups them by wave type and count occurences.
    """
    premature_res = df.loc[(df.tag1 == 'premature') | (df.tag2 == 'premature') | (df.tag3 == 'premature')].groupby('type').size()
    return {
        'QRS': premature_res.get('QRS', 0),
        'P': premature_res.get('P', 0)
    }

def get_avg_heart_rate(data):
    """
    Calculates the mean heart rate by counting the total QRS waves number
    in the record and dividing it by the time delta (last start - first start) 
    """
    time_delta = data.iloc[-1].start - data.iloc[0].start
    avg = 0
    try:
        avg = (data.groupby('type').size().get('QRS', 0) / (time_delta / 1000)) * 60
    except ZeroDivisionError:
        print('\nError : It looks like the record has a null duration')
    return int(avg)

def get_min_and_max_heart_rate(data, start_dt):
    """
    First adds a column with the start of all QRS events at datetime format (taking user input into account).
    Then uses this column to count QRS in all 10 minutes ranges, and returns 
    a dictionary storing the min and max row.
    The shorter the range time is, the most precise the results are, but it can't
    be too short to avoid getting only extreme results.
    """
    df = data.loc[(data.type == 'QRS')]
    start_dt = parser.parse(start_dt)
    # Add a datetime column to the dataframe, with start datetime for each row.
    # Could probably be improved in terms of performance
    df['start_dt'] = df.apply(lambda x: pd.to_datetime(datetime.datetime.strftime(
        start_dt + relativedelta.relativedelta(microseconds=x.start*1000), '%Y-%m-%d %H:%M:%S.%f')), axis=1
        )
    try:
        df = df.set_index('start_dt').resample(str(INSTANT_FREQ_RANGE)+'min').count()
        min_row = df.loc[df.type == df['type'].min()].iloc[0]
        max_row = df.loc[df.type == df['type'].max()].iloc[0]
        # min_row /max_row are dataframes with all columns having the same value,
        # which is the number of QRS occurences for the INSTANT_FREQ_RANGE time range, at date min(max)_row.name
        min_dict = {
            'Frequency': int(int(min_row.type) / INSTANT_FREQ_RANGE),
            'Date': str(min_row.name)
        }
        max_dict = {
            'Frequency': int(int(max_row.type) / INSTANT_FREQ_RANGE),
            'Date': str(max_row.name)
        }
    except (ZeroDivisionError, ValueError):
        print('\nError : The INSTANT_FREQ_RANGE parameter must be a strictly positive number. Returning -1 for min and max.')
        return {'min': -1, 'max': -1}
    return {
        'min': min_dict,
        'max': max_dict
    }

def get_heart_rate_indicators(data, start_dt):
    min_and_max_heart_rates = get_min_and_max_heart_rate(data, start_dt)
    return {
        'Average heart rate': get_avg_heart_rate(data),
        'Minimum heart rate': min_and_max_heart_rates['min'],
        'Maximum heart rate': min_and_max_heart_rates['max']
    }

def run(fpath, start_dt):
    results = {}
    try:
        df = pd.read_csv(fpath, names=HEADER).fillna('')
    except Exception as e:
        print('\nError : It seems the file you supplied contains invalid data.')
        print('The following exception occured: {}'.format(e))
        return
    results['Premature waves'] = get_premature_data(df)
    results['Heart rate indicators'] = get_heart_rate_indicators( df, start_dt)
    print_report(results)

def check_inputs(fpath, startdt):
    valid = True
    try:
        parser.parse(start_dt)
    except ValueError:
        print('\nError : Could not parse the supplied date.')
        valid = False
    try:
        open(fpath, 'r')
    except FileNotFoundError:
        print('\nError : The file %s doesn\'t exist.' % fpath)
        valid = False
    return valid

if __name__ == '__main__':
    valid_inputs = False
    while not valid_inputs:
        fpath = input('Enter the file path : \n')
        start_dt = input('Enter the record start date\n')
        valid_inputs = check_inputs(fpath, start_dt)
    run(fpath, start_dt)

