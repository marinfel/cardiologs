import unittest
import sys
import pandas as pd
sys.path.insert(0,'..')
import cardiologs


class TestHolterSummary(unittest.TestCase):
    """
    Simple test cases for functions defined in cardiologs.py.
    """

    def setUp(self):
        """
        Test cases use a known file.
        As we know it content, we know what module functions should return.
        """
        # Take only a one minute range to compute min and max HR as the test file doesn't last 24h
        cardiologs.INSTANT_FREQ_RANGE = 0.15
        self.date = '2000-01-01'
        self.df = pd.read_csv('test.csv', names=cardiologs.HEADER).fillna('')

    def test_premature_data(self):
        """
        Test premature data computation 
        In the test file, we have 2 premature QRS and 1 premature P wave
        """
        res = cardiologs.get_premature_data(self.df)
        expected = {
            'QRS': 2,
            'P': 1
        }
        self.assertEqual(res, expected,
                          'The file contains 2 prematures QRS waves and 1 premature P wave.')

    def test_avg_heart_rate(self):
        """
        Test the average heart rate returned by the programm.
        In the test file, the first wave occures at 0 ms. The last one occures at 26 000 ms
        and we have 4 QRS waves in the file. 4 * 60 / 26 ~= 9.23. We only return the ceil so
        result should be 9.
        """
        res = cardiologs.get_avg_heart_rate(self.df)
        self.assertEqual(res, 9, 'Expected an average heart rate of 10 BPM from test file')

    def test_min_and_heart_rate(self):
        """
        Test min and max heart rate.
        The time range used to group QRS is 0.15 minutes.
        Therefore we have the following counts by 0.15 min range:
            2000-01-01 00:00:00     1
            2000-01-01 00:00:09     2
            2000-01-01 00:00:18     1
        This gives a min frequency of int(1/0.15) = 6 bpm that starts at 2000-01-01 00:00:00
        and a max frequency of 2/0.15 = 13 bpm, that starts at 2000-01-01 00:00:09
        """
        res = cardiologs.get_min_and_max_heart_rate(self.df, self.date)
        expected = {
            'min':
                {'Frequency': 6, 'Date': '2000-01-01 00:00:00'},
            'max':
                {'Frequency': 13, 'Date': '2000-01-01 00:00:09'}
        }
        self.assertEqual(res, expected, 'Expected %s, got %s' % (expected, res,))
        # Non passing case: negative range parameter
        cardiologs.INSTANT_FREQ_RANGE = -10
        res = cardiologs.get_min_and_max_heart_rate(self.df, self.date)
        self.assertEqual(res, {'min': -1, 'max': -1})

if __name__ == '__main__':
    unittest.main()

