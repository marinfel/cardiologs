# README


## Content of this repository
You will find in this repository:
- a cardiologs.py file, which runs the application
- a record.csv file that I was given to analyse
- a test folder, that contains some unit tests.

## Running the app


To run the app, you first need to install the requirements:
```bash
sudo pip install -r requirements.txt
```
Then simply run the following command :
```bash
python cardiologs.py
```

To run unit tests, run the following command :
```bash
cd tests
python test.py
```

## Bonus question

The question was to provide a solution that allows users to download a sample of a record, giving as parameters a time range.
Here is what could be done according to me:

- I would use a NoSQL document oriented database to store the delineations data, as all wave data can have a variable number of tags.
In MongoDB for example, we could use one collection storing one ECG.
- On a client side, I would have the user giving needed information, such as the ECG he/she wants to retrieve, the record start date and the time range.
It could be supplied as a list of parameters (days=X, hours=Y, seconds=Z, ...) - just like a python relativedelta object.
- On the server side, a REST API would filter database documents using waves onset and offset.
- The response would either be displayed on the client interface or sent as a csv file, depending on the expressed need.
